﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.Recorder;
using UnityEditor.Presets;
using UnityEditor;
using UnityEditor.Recorder.Input;

public class LaunchRecorder : MonoBehaviour
{
    private RecorderControllerSettings recSetting;
    private RecorderController recContro;
    private RecorderWindow recWin;
    public const string MenuRoot = "Window/Recorder/";
    public const int MenuRootIndex = 2050;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Start!!");
        //recSetting = new RecorderControllerSettings();
        //recSetting.CapFrameRate = true;
        //recSetting.FrameRate = 50f;
        //Debug.Log(recSetting.FrameRate);
        recSetting = RecorderControllerSettings.LoadOrCreate("C:\\Users\\julien\\Documents\\Unity\\Test Cinemachine\\Assets\\Test\\MovieRecorderSettings.preset");

        recContro = new RecorderController(recSetting);

        //Debug.Log(recContro.Settings);
        //Debug.Log(recContro.Settings.FrameRate);
        //Debug.Log(recContro.Settings.CapFrameRate);
        //Debug.Log(recContro.Settings.FrameRatePlayback);
        //Debug.Log(recContro.Settings.RecorderSettings);
        

        //recWin = (RecorderWindow)EditorWindow.GetWindow(typeof(RecorderWindow));
        //recWin.StartRecording();

        StartPersonnalRecorder();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("Space key was pressed.");
            recWin.StartRecording();
            recContro.PrepareRecording();
            recContro.StartRecording();

        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            Debug.Log("A key was pressed.");
            recWin.StopRecording();
            recContro.StopRecording();
            //recWin.StartRecording();
        }
    }

    static void StartPersonnalRecorder()
    {
        Debug.Log("Method call");

        //Test code
        var controllerSettings = ScriptableObject.CreateInstance<RecorderControllerSettings>();
        var TestRecorderController = new RecorderController(controllerSettings);

        var videoRecorder = ScriptableObject.CreateInstance<MovieRecorderSettings>();
        videoRecorder.name = "My Video Recorder";
        videoRecorder.Enabled = true;
        videoRecorder.VideoBitRateMode = VideoBitrateMode.High;

        videoRecorder.ImageInputSettings = new GameViewInputSettings
        {
            OutputWidth = 1920,
            OutputHeight = 1080
        };

        videoRecorder.AudioInputSettings.PreserveAudio = true;
        //videoRecorder.OutputFile; // Change this to change the output file name (no extension)

        controllerSettings.AddRecorderSettings(videoRecorder);
        controllerSettings.SetRecordModeToFrameInterval(0, 119); // 4s @ 30 FPS
        controllerSettings.FrameRate = 30;

        RecorderOptions.VerboseMode = false;
        TestRecorderController.PrepareRecording();
        TestRecorderController.StartRecording();
    }

    public const string PlayMe = "PlayMe";


    public class PlaySomeMonoBehaviour : MonoBehaviour
    {


        public string WhatToDo = PlayMe;


        void Start() { switch (WhatToDo) { case PlayMe: StartCoroutine((YetSomeOtherClass.DoSomething())); break; } }
    }

    [MenuItem("Play/PlayMe")]
    public static void BuildAssetBundles()
    {
        EditorApplication.isPlaying = true;
        EditorApplication.NewScene();
        GameObject go = new GameObject("PlayMode");
        go.AddComponent<PlaySomeMonoBehaviour>().WhatToDo =
            PlaySomeMonoBehaviour.PlayMe;
    }
}
